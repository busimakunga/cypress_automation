<h1>QA Automation Testing</h1>

**Install**

* NodeJS
* Cypress
* IDE of your choice, like Visual Studio Code

**Setup**

* Create a /repo directory (`mkdir repo`)
* In terminal change to the /repo directory (`cd repo`)
* Clone the project to your /repo directory (`git clone git@gitlab.com:busimakunga/cypress_automation.git`)

* Open the project up in VS Code and `run npm install`

* If you are running Cypress for the first time run `npx cypress verify`

**Executing Tests** <br>
* To execute tests execute `npx cypress open` 
* Select E2E Testing
* Select Chrome
* Click on Start E2E Testing in Chrome
* Select assessment.cy.js spec to execute the test cases

**Notes** <br>
Tests are located in cypress/e2e <br>
In order to run headless test execute `npx cypress run`
